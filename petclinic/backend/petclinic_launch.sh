#!/bin/bash
# chkconfig: 345 90 90
# description: Manage PetClinic

case $1 in
  start)  # Start Vault
          cd /spring-petclinic
          touch petclinic.pid
          nohup ./mvnw spring-boot:run &
          sleep 10
          ps -ef | grep 'spring-petclinic' | awk '{print $2}' > /spring-petclinic/petclinic.pid
          ;;
  stop)   # Kill you script here
	  if [ -e /home/ubuntu/spring-petclinic/petclinic.pid ]
	  then
          	kill $(cat /spring-petclinic/petclinic.pid)
	  fi
          ;;

  status) # Show something useful
          ps -ef | grep -v grep | grep 'spring-petclinic' >/dev/null 2>/dev/null
	  if (( $? == 0 ))
	  then
		echo "PetClinic is running"
	  else
		echo "PetClinic is not running"
	  fi
          ;;
  *) # Some default message
     echo "Use either start, stop or status"
     ;;
esac